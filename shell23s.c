// Name: Chintal Devanshubhai Raval
// Section: 2
// ID: 110096546

// Header for displaying input output on terminal
#include<stdio.h>
// Lib for exit like functions
#include<stdlib.h>
// For opening and getting the file descriptors
#include<fcntl.h>
#include<unistd.h>
// String manipulation functions
#include<string.h>
// Working with true, false variables
#include<stdbool.h>
// for waiting for a child
#include<sys/wait.h>


// chr arr for reading the input
char cust_argv[10000];
// arguments num after seperation
int cur_ar_c = 0;
// chr arr after seperating input
char *arg_arr_sep[10000];
// var to store num of special characters
int cnt_of_spec_chr = 0;
// chosen special operation
char *chosen_ope = NULL;
// boolean var, stays true only when error arrises
bool er_oc = false;
// indexes of special chars in the input
int spec_chr_indexes[] = {-1, -1, -1, -1, -1, -1, -1};
int spec_chr_indexes_cnt = 0;
bool is_spec_chr_provided = false;
bool avoid_er = false;
bool prg_okay_to_run = false;

// int case for knowing which spec chr case to run
int case_ = 0;
int wait_st;

// num of & cnt provided
int amp_cnt = 0;

// 2D array for storing command and its args
char *case_args_arr[8][5];
int case_args_cnt = 0;

void print_prompt(){
    printf("\nmshell$ ");
}

void show_er(){
    fprintf(stderr, "Argumnets not valid. Please check.\n");
    er_oc = true;
}

void show_exe_er(){
    fprintf(stderr, "Something went wrong while executing.\n");
    er_oc = true;
}

// method that returns bool if the given str is spec ope
bool is_spec_chr(char *sent_val){
    // implementing strcmp func
    if((strcmp(sent_val, "|") == 0) || (strcmp(sent_val, "||") == 0) || (strcmp(sent_val, "&&") == 0) || (strcmp(sent_val, ";") == 0) || (strcmp(sent_val, ">") == 0) || (strcmp(sent_val, "<") == 0) || (strcmp(sent_val, ">>") == 0) || (strcmp(sent_val, "&") == 0)){
        // is equal to 0 means match
        return true;
    }
    return false;
}

// populate 2d array, row and col is given during calling of funct 
void fill_spec_args_arr(char *arg, int col, int row){
    case_args_arr[row][col] = strdup(arg);
}

// function that checks only 1 type of ope is provided
bool verifY_ope(char *cur_ar){
    is_spec_chr_provided = true;
    bool ope_ok = false;
    if(chosen_ope == NULL){
        // var is NULL first and funct called
        ope_ok = true;
        // set the chosen_ope to provided strn
        chosen_ope = cur_ar;
    }
    // piping
    else if((strcmp(chosen_ope, "|") == 0) && ((strcmp(chosen_ope, cur_ar) == 0))){
        // given str and prev ope is same
        ope_ok = true;
    }
    // redirection, compare chosen Ope first
    else if((strcmp(chosen_ope, ">") == 0) || (strcmp(chosen_ope, "<") == 0) || (strcmp(chosen_ope, ">>") == 0)){
        // check provied str
        if((strcmp(cur_ar, ">") == 0) || (strcmp(cur_ar, "<") == 0) || (strcmp(cur_ar, ">>") == 0)){
            ope_ok = true;
        }
    }
    // conditional, compare provided ope first
    else if((strcmp(chosen_ope, "&&") == 0) || (strcmp(chosen_ope, "||") == 0)){
        // check passed str
        if((strcmp(cur_ar, "&&") == 0) || (strcmp(cur_ar, "||") == 0)){
            ope_ok = true;
        }
    }
    // for background processing
    else if((strcmp(chosen_ope, "&") == 0) && ((strcmp(chosen_ope, cur_ar) == 0))){
        ope_ok = true;
    }
    // sequential execution
    else if((strcmp(chosen_ope, ";") == 0) && ((strcmp(chosen_ope, cur_ar) == 0))){
        ope_ok = true;
    }
    return ope_ok;
}

// validate if given str is special chr
void check_spec_chars(char *cur_ar){    
    // pipe
    if(strcmp(cur_ar, "|") == 0){
        // verify if its valid
        if(verifY_ope(cur_ar)){
            // increase count
            cnt_of_spec_chr++;
            spec_chr_indexes[spec_chr_indexes_cnt] = cur_ar_c;
            spec_chr_indexes_cnt++;
            // case 1 for pipe
            case_ = 1;
        }
        else{
            // not valid, show error
            show_er();
            return;
        }
    }
    // redirection
    else if((strcmp(cur_ar, ">") == 0) || (strcmp(cur_ar, "<") == 0) || (strcmp(cur_ar, ">>") == 0)){
        // test if its valid
        if(verifY_ope(cur_ar)){
            // increment count
            cnt_of_spec_chr++;
            spec_chr_indexes[spec_chr_indexes_cnt] = cur_ar_c;
            spec_chr_indexes_cnt++;
            // case 2 for redirection
            case_ = 2;
        }
        else{
            // not valid, show error
            show_er();
            return;
        }
    }
    // Conditional exe case
    else if((strcmp(cur_ar, "&&") == 0) || (strcmp(cur_ar, "||") == 0)){
        // check validity
        if(verifY_ope(cur_ar)){
            // increment count
            cnt_of_spec_chr++;
            spec_chr_indexes[spec_chr_indexes_cnt] = cur_ar_c;
            spec_chr_indexes_cnt++;
            // case 3 for conditional
            case_ = 3;
        }
        else{
            // not valid, show error
            show_er();
            return;
        }
    }
    // bg prc case
    else if((strcmp(cur_ar, "&") == 0)){
        // check validity
        if(verifY_ope(cur_ar)){
            // raise count
            cnt_of_spec_chr++;
            spec_chr_indexes[spec_chr_indexes_cnt] = cur_ar_c;
            spec_chr_indexes_cnt++;
            if(spec_chr_indexes_cnt > 1){
                // should be only 1 &, more than 1 triggers error
                show_er();
                return;
            }
            // case 4 for Background case
            case_ = 4;
            // inc. amp count
            amp_cnt++;
        }
        else{
            // not valid, show error
            show_er();
            return;
        }
    }
    // sequence case 
    else if((strcmp(cur_ar, ";") == 0)){
        if(verifY_ope(cur_ar)){
            // set case int for seq
            case_ = 5;
            cnt_of_spec_chr++;
            // inc counter
            spec_chr_indexes[spec_chr_indexes_cnt] = cur_ar_c;
            spec_chr_indexes_cnt++;
        }
        else{
            // not valid, show error
            show_er();
            return;
        }
    }
    // rest of the cases are not valid
    else if(strcmp(cur_ar, "<<") == 0){
        show_er();
        return;
    }
    // character count not valid
    if(cnt_of_spec_chr > 7){
        show_er();
        return;
    }
}

// func that checks and returns bool based on num of args
bool num_arg_check(int arg){
    if((arg < 1 || arg > 5)){
        // not valid
        show_er();
        return false;
    }
    else if(avoid_er && arg > 5){
        // not valid
        show_er();
        return false;
    }
    // valid
    return true;
}

// Function that processes the arr
void seprate_input(){
    // store str in curr_ar 
    char *curr_ar = strtok(cust_argv, " "); // this fun returns token based on delimiter, here it is space
    char *ar_for_cnt = strdup(curr_ar);     // strdup allows for duplicating str
    int er_bool = false;        // local er bool
    while(curr_ar != NULL){     // while curr_ar is not null
        // check if its spec chr or not
        check_spec_chars(curr_ar);
        // processing is done within nested function
        if(er_oc == true || er_bool == true){
            show_er();
            return;
        }
        if(amp_cnt == 1){
            // amperson count 1, meaning 2 provided, set error true
            er_bool = true;
        }
        // add value to arr
        arg_arr_sep[cur_ar_c] = strdup(curr_ar);        // using this to make replica of string
        cur_ar_c = cur_ar_c + 1;        // inc count
        curr_ar = strtok(NULL, " ");          // get next token
    }
    if(is_spec_chr_provided){
        // special chr porvided
        int idx = 0;        // idx is for the arguments of ind command
        int special_counter = 0;
        // set local index and counter
        for(int i = 0; i < cur_ar_c; i++){  // loop till every token is processed
            char *arg = strdup(arg_arr_sep[i]);     // make copy of it
            if(!is_spec_chr(arg)){
                // not a spec chr, populate 2d arr
                fill_spec_args_arr(arg, idx, special_counter);
                // inc counter
                idx++;
            }
            else{
                // it is a spec chr
                special_counter++;
                idx = 0;
                // inc counter, set idx to 0
            }  
        }
    }

    if(spec_chr_indexes[0] == 0){
        // scenario when first arg is a spec chr, not valid
        show_er();
        // raise funct
        return; 
    }
    if(spec_chr_indexes[spec_chr_indexes_cnt - 1] == (cur_ar_c - 1)){
        // last arg is a spec chr
        if((strcmp(arg_arr_sep[cur_ar_c-1], "&") != 0) && (spec_chr_indexes_cnt - 1) != -1){    // only & is allowed as last spec chr
            // not valid scenario
            show_er();
            // raise funct
            return;    
        }
        else{
            // scenario true, avoid er
            avoid_er = true;;
        }
    }
    int ind_arg = -1;
    if(is_spec_chr_provided){
        bool left_args_counted = false;
        // bool for checking left args are processed or not
        for(int i = 0; i < spec_chr_indexes_cnt; i++){      // loop over spec chr indxes
            if(spec_chr_indexes_cnt == 1){
                ind_arg = spec_chr_indexes[0];
                // first arg
                if(!num_arg_check(ind_arg)){
                    // check numbers
                    return;
                }
                if(!avoid_er){
                    // if avoid er false
                    ind_arg  = cur_ar_c - spec_chr_indexes[0] - 1;
                    // check number of args
                    if(!num_arg_check(ind_arg)){
                        return;
                    }
                }
            }
            else{    
                if(i == 0){     // first element
                    ind_arg = spec_chr_indexes[i];  // get individual args for first element
                    if(!num_arg_check(ind_arg)){
                        // not valid
                        return;
                    }
                }
                else if(i == (spec_chr_indexes_cnt - 1 )){      // index matches val
                        if(!left_args_counted){
                        // left Args not counted
                        ind_arg = spec_chr_indexes[i] - spec_chr_indexes[i-1] - 1;    
                        if(!num_arg_check(ind_arg)){
                            // not valid
                            return;
                        }
                        // set to true
                        left_args_counted = true; 
                    }
                }
                else{
                    // get number
                    ind_arg = spec_chr_indexes[i] - spec_chr_indexes[i-1] - 1;
                    if(!num_arg_check(ind_arg)){
                    // mnot valid
                    return;
                }
                }
            }
        }
        if(left_args_counted){
            // left args counted
            ind_arg = cur_ar_c - spec_chr_indexes[spec_chr_indexes_cnt - 1] - 1;       // Count right side args
            if(!num_arg_check(ind_arg)){
                    // not valid
                    return;
                }
        }
    }
    else{
        ind_arg = cur_ar_c;     // last scenario
        if(!num_arg_check(ind_arg)){        // not valid
            return;
        }
    }
    // all args valid, okay to run
    prg_okay_to_run = true;
}

// function that reset old vals
void wipe_structures(){
    // first rg set to null
    cust_argv[0] = '\0';
    for (int i = 0; i < cur_ar_c; i++) {
        // release mem
        free(arg_arr_sep[i]);
    }
    for (int i = 0; i < cur_ar_c; i++) {
        // set to null
        arg_arr_sep[i] = NULL;
    }
    cur_ar_c = 0;
    er_oc = false;
    chosen_ope = NULL;
    cnt_of_spec_chr = 0;
    for(int i = 0; i < 7; i++){
        spec_chr_indexes[i] = -1;
    }
    spec_chr_indexes_cnt = 0;
    is_spec_chr_provided = false;
    avoid_er = false;
    prg_okay_to_run = false;
    case_ = 0;
    amp_cnt = 0;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 5; j++){
            case_args_arr[i][j] = NULL;
        }
    }
}

// basic function with no spec chr
void no_spec_arg_case(){
    if(fork() > 0){
        // parent waits
        wait(&wait_st);
    }
    else{
        // create data structs
        int args = cur_ar_c - 1;
        char *pg_n = arg_arr_sep[0];
        char *pg_args[cur_ar_c + 1];
        pg_args[0] = pg_n;
        pg_args[cur_ar_c] = NULL;
        for(int j = 1; j < cur_ar_c; j++){
            pg_args[j] = arg_arr_sep[j];
        }
        // exe command
        execvp(pg_n, pg_args);
        exit(0);
    }
    return;    
}

// sequential execution case
void seq_exec_case(){
    // iterate over 2d arr
    for (int row = 0; row < 8; row++) {
        // check for null
        if (case_args_arr[row][0] != NULL) {        // not null means provided
            char *program = case_args_arr[row][0];
            char *args[5]; 
            args[0] = case_args_arr[row][0]; 
            int last_el = 0;
            for (int col = 1; col < 5 && case_args_arr[row][col] != NULL; col++) {
                args[col] = case_args_arr[row][col];
                last_el = col;
            }
            args[last_el + 1] = NULL;
            // build program for exec* till here
            int seq_chlds = fork();
            if (seq_chlds == 0) {
                // Child process
                execvp(program, args);
                fprintf(stderr, "an errord occured, could not replace bash");  // Print an error message if execvp fails
                return;  // Terminate child process on error
            } 
            else if (seq_chlds < 0) {
                fprintf(stderr, "fork failed");
                return;
            }
            else{
                int status;
                waitpid(seq_chlds, &status, 0);
            } 
        }
    }
    // parent waiting for chld
    for (int row = 0; row < 8; row++) {
        wait(NULL);
    }
}

// funct for background exec
void bg_exec_case(){
    for (int row = 0; row < 8; row++) {     // iterate over 2d arr
        if (case_args_arr[row][0] != NULL) {
            // same logic, build exec arrr
            char *program = case_args_arr[row][0];
            char *args[5]; 
            args[0] = case_args_arr[row][0]; 
            int last_el = 0;
            for (int col = 1; col < 5 && case_args_arr[row][col] != NULL; col++) {
                args[col] = case_args_arr[row][col];
                last_el = col;
            }
            args[last_el + 1] = NULL;
            int pid = fork();
            if (pid == 0) {
                // First child process
                int second_child_pid = fork();
                if (second_child_pid == 0) {
                    // Second child process
                    int out_fd = open("/dev/null", O_WRONLY);
                    // no need to display output of bg prc
                    dup2(out_fd, 1);
                    // replicate fd and close file
                    close(out_fd);
                    execvp(program, args); 
                    // exec command
                    return;
                } 
                else if (second_child_pid < 0) {
                    fprintf(stderr, "forking error\n");
                    return;
                }
                else{
                    exit(0);
                }
            }
            else if (pid < 0) {
                fprintf(stderr, "forking error\n");
                return;
            } 
            else {
                waitpid(pid, &wait_st, 0);
                // parent prc
                printf("Moved process %d to bg\n", pid);    // print id
            }
        }
    }
}

// funct for conditional case
void execute_command(char *program, char *args[], int in_fd, int out_fd) {
    if (fork() == 0) {
        // based on scenario, change descriptors
        if (in_fd != STDIN_FILENO) {
            dup2(in_fd, STDIN_FILENO);
            // input redirection
            close(in_fd);
        }
        if (out_fd != STDOUT_FILENO) {
            dup2(out_fd, STDOUT_FILENO);
            // output redirection
            close(out_fd);
        }
        // exec command
        execvp(program, args);
    } 
    else {
        // parent waits
        wait(NULL);
    }
}

// case for redirection
void redi_exec_case(){
    // cant have more than 1 redi chr
    for(int row = 0; row < 8; row++){
        if(row == 2 && case_args_arr[row][0] != NULL){
            show_exe_er();
            // more than 1 found show err
            printf("Can not have more than 1 redirection character.\n");
            return;
        }
    }
    // okay to run
    for (int row = 0; row < 1; row++) {
        if (case_args_arr[row][0] != NULL) {
            char *program1 = case_args_arr[row][0];
            char *args1[] = {NULL, NULL, NULL, NULL, NULL, NULL};
            for (int col = 0; col < 5 && case_args_arr[row][col] != NULL; col++) {
                args1[col] = case_args_arr[row][col];
                if(case_args_arr[row][col+1] == NULL){
                    args1[col+1] = NULL;
                }
            }

            char *program2 = case_args_arr[row+1][0];
            char *args2[] = {NULL, NULL, NULL, NULL, NULL, NULL};
            for (int col = 0; col < 5 && case_args_arr[row+1][col] != NULL; col++) {
                args2[col] = case_args_arr[row+1][col];
                if(case_args_arr[row+1][col+1] == NULL){
                    args2[col+1] = NULL;
                }
            }
            // execv arr process done

            int in_fd, out_fd;
            // set in/out fd based on arrow
            if(strcmp(chosen_ope, ">") == 0){
                in_fd = STDIN_FILENO;
                // output redirection
                out_fd = open(args2[0], O_CREAT | O_RDWR, 0777);
                if(out_fd == -1){
                    show_exe_er();
                    fprintf(stderr, "Could not open %s file\n", args2[0]);
                    return;
                }
                execute_command(program1, args1, in_fd, out_fd);
                close(out_fd);
            }
            else if(strcmp(chosen_ope, "<") == 0){
                in_fd = open(args2[0], O_RDONLY);
                // input redirection
                out_fd = STDOUT_FILENO;
                if(in_fd == -1){
                    show_exe_er();
                    fprintf(stderr, "Could not open %s file\nCheck if it exists or not.", args2[0]);
                    return;
                }
                execute_command(program1, args1, in_fd, out_fd);
                close(in_fd);
            }
            else if(strcmp(chosen_ope, ">>") == 0){
                in_fd = STDIN_FILENO;
                // case where we append
                out_fd = open(args2[0], O_WRONLY | O_APPEND | O_CREAT, 0777);
                if(out_fd == -1){
                    show_exe_er();
                    fprintf(stderr, "Could not open %s file\n", args2[0]);
                    return;
                }
                execute_command(program1, args1, in_fd, out_fd);
                close(out_fd);
            }
        }
    }
}

// case for pipe
void pip_exec_case() {
    int pipefds[2];
    // need pipe here
    int prev_pipefd = -1;
    // set lasr pipe descriptor
    for (int row = 0; row < spec_chr_indexes_cnt + 1; row++) {
        if (case_args_arr[row][0] != NULL) {
            char* program = case_args_arr[row][0];
            char* args[6];
            args[0] = case_args_arr[row][0];
            int last_el = 0;
            for (int col = 1; col < 5 && case_args_arr[row][col] != NULL; col++) {
                args[col] = case_args_arr[row][col];
                last_el = col;
            }
            args[last_el + 1] = NULL;
            if (pipe(pipefds) == -1) {
                // create pipe
                show_exe_er();
                fprintf(stderr, "Error creating pipe\n");
                return;
            }
            // build execv arr till now
            int pid = fork();
            if (pid == 0) {
                // child
                if (prev_pipefd != -1) {
                    // no previouse pipe found
                    if (dup2(prev_pipefd, STDIN_FILENO) == -1) {
                        fprintf(stderr, "dup2 error occcured");
                        return;
                    }
                    close(prev_pipefd);
                }
                if (row < spec_chr_indexes_cnt) {
                    // dup std out
                    dup2(pipefds[1], STDOUT_FILENO);
                    close(pipefds[1]);
                }
                // exec command
                execvp(program, args);
                return;
            } 
            else {
                if (prev_pipefd != -1) {
                    // last pipe used
                    close(prev_pipefd);
                }
                if (row < spec_chr_indexes_cnt) {
                    // close writing end
                    close(pipefds[1]);
                    prev_pipefd = pipefds[0];
                    // change var
                }
                // parent waits
                wait(NULL);
            }
        }
    }
}

// check and return bool based on exec command execution
bool exec_condi_command(char *program, char *args[]) {
    int condi_p_id = fork();
    if (condi_p_id == 0) {
        // child here, run exec command
        if(execvp(program, args) == -1){
            // error occured
            return false;
        }
    } 
    else {
        int status;
        waitpid(condi_p_id, &status, 0);
        // need to check if command generated error or was successfull
        if(WIFEXITED(status) && WEXITSTATUS(status) == 0) {     // using exit status
            // was finished with exit status 0
            return true;
        } 
        else {
            // else case, status was not 0
            return false;
        }
    }
}

// case for conditional execution
void condi_exec_case(){
    // local vars
    int cnt = 0;
    bool prev_success = false;
    // first time running or not
    bool first_run = true;
    // last executed operation
    char *last_ope = NULL;
    for (int row = 0; row < 8; row++) {
        if (case_args_arr[row][0] != NULL && (cnt <= spec_chr_indexes_cnt)) {
            char *program1 = case_args_arr[row][0];
            char *args1[] = {NULL, NULL, NULL, NULL, NULL, NULL};
            for (int col = 0; col < 5 && case_args_arr[row][col] != NULL; col++) {
                args1[col] = case_args_arr[row][col];
                if(case_args_arr[row][col+1] == NULL){
                    args1[col+1] = NULL;
                }
            }
            char *program2 = case_args_arr[row+1][0];
            char *args2[] = {NULL, NULL, NULL, NULL, NULL, NULL};
            for (int col = 0; col < 5 && case_args_arr[row+1][col] != NULL; col++) {
                args2[col] = case_args_arr[row+1][col];
                if(case_args_arr[row+1][col+1] == NULL){
                    args2[col+1] = NULL;
                }
            }
            char *ope = arg_arr_sep[spec_chr_indexes[cnt]];
            if(!ope){
                // operation null, finished execution
                return;
            }
            if(strcmp(ope, "&&") == 0){     // And case
                if(first_run){      // First run of the program
                    bool success = exec_condi_command(program1, args1);     // exec command
                    first_run = false;      // First run set to false
                    if(success){
                        // first program was succ
                        prev_success = exec_condi_command(program2, args2);     // exec 2nd command
                        row++;      // inc row for next iteration
                    }
                }
                else if(!first_run){
                    // not the first run
                    if(prev_success){
                        // check if prev command was true or not
                        prev_success = exec_condi_command(program1, args1);
                    }
                }
                
            }
            else if(strcmp(ope, "||") == 0){        // OR CASE
                if(first_run){
                    // first run
                    bool success = exec_condi_command(program1, args1);
                    first_run = false;  
                    if(!success){
                        // not success, exec 2nd command
                        prev_success = exec_condi_command(program2, args2);
                        row++;  // inc counter
                    }
                    else{
                        // first program ran, set true
                        prev_success = true;    
                        row++;  // inc counter
                    }
                }
                else if(!first_run){
                    // or case 2nd run
                    if(!prev_success){
                        // if prev is false, then only run this
                        prev_success = exec_condi_command(program1, args1);
                    }
                }
            }
            cnt++;      // inc counter
        }
    }
}

// funct that runs corresponding command
void run_prg(){
    switch(case_){
        // based on case, call specific ommand
        case 1:
            pip_exec_case();
            break;

        case 2:
            redi_exec_case();
            break;

        case 3:
            condi_exec_case();
            break;

        case 4:
            bg_exec_case();
            break;

        case 5:
            seq_exec_case();
            break;

        default:
            no_spec_arg_case();
            break;
    }
}

// Main funct
int main(){
    for(;;){        // this runs infinite loop
        print_prompt();
        // print promot
        scanf(" %[^\n]", cust_argv);        // scan value and store it in arr
        seprate_input();        // process arr
        if(er_oc == true || prg_okay_to_run == false){
            // error occured, set vals to default
            wipe_structures();
            continue;       // used to keep running loop
        }
        else if(prg_okay_to_run){
            run_prg();
            if(er_oc){
                // err occured during exec, set values to normal
                wipe_structures();
                continue;       // continue the next iter
            }
        }
        // program ran normally, set value back to default
        wipe_structures();
    }
    // never reaches here, but require for int return type.
    return 0;
}